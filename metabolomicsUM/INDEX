aov.all.vars            Analysis of variance
aov.one.var             Analysis of variance of one variable
apply.filter.function   Apply filter function
background.correction   Background correction
baseline.correction     Baseline correction
boxplot.variables       Boxplot of variables
calculate.ellipses      Calculate ellipses
calculate.shifts        Calculate shifts
check.dataset           Check dataset
clustering              Perform cluster analysis
convert.to.factor       Convert metadata to factor
correlations.dataset    Dataset correlations
count.missing.values    Count missing values
count.missing.values.per.sample
                        Count missing values per sample
count.missing.values.per.variable
                        Count missing values per variable
create.dataset          Create dataset
data.correction         Data correction
dataset.from.peaks      Dataset from peaks
dendrogram.plot         Plot dendrogram
dendrogram.plot.col     Plot dendrogram
feature.selection       Perform feature selection
filter.feature.selection
                        Perform selection by filter
find.equal.samples      Find equal samples
first.derivative        First derivative
flat.pattern.filter     Flat pattern filter
flat.pattern.filter.percentage
                        Flat pattern filter by percentage
flat.pattern.filter.threshold
                        Flat pattern filter by threshold
fold.change             Fold change analysis
get.all.intensities     Get all intensities.
get.data                Get data
get.data.as.df          Get data as data frame
get.data.value          Get data value
get.data.values         Get data values
get.intensity           Get intensity
get.metadata            Get metadata
get.metadata.value      Get metadata value
get.metadata.var        Get metadata variable
get.overall.freq.list   Get overall frequencies list
get.peak.values         Get peak values
get.sample.names        Get sample names
get.type                Get type of data
get.value.label         Get value label
get.x.label             Get x-axis label
get.x.values.as.num     Get x-axis values as numbers
get.x.values.as.text    Get x-axis values as text
heatmap.correlations    Correlations heatmap
hierarchical.clustering
                        Perform hierarchical clustering analysis
impute.nas.knn          Impute missing values with KNN
impute.nas.linapprox    Impute missing values with linear approximation
impute.nas.mean         Impute missing values with mean
impute.nas.median       Impute missing values with median
impute.nas.value        Impute missing values with value replacement
is.spectra              Check type of data
kmeans.clustering       Perform k-means clustering analysis
kmeans.plot             Plot kmeans clusters
kmeans.result.df        Show cluster's members
merge.datasets          Merge two datasets
metadata.as.variables   Metadata as variables
missingvalues.imputation
                        Missing values imputation
msc.correction          Multiplicative scatter correction
multiClassSummary       Multi Class Summary
multifactor.aov.all.vars
                        Multifactor ANOVA
multifactor.aov.onevar
                        One Variable Multifactor ANOVA
normalize               Normalize data
num.samples             Get number of samples
num.x.values            Get number of x values
offset.correction       Offset correction
pca.analysis.dataset    PCA analysis (classical)
pca.biplot              PCA biplot
pca.biplot3D            3D PCA biplot (interactive)
pca.importance          PCA importance
pca.kmeans.plot2D       2D PCA k-means plot
pca.kmeans.plot3D       3D PCA k-means plot (interactive)
pca.pairs.kmeans.plot   PCA k-means pairs plot
pca.pairs.plot          PCA pairs plot
pca.plot.3d             3D pca plot
pca.robust              PCA analysis (robust)
pca.scoresplot2D        2D PCA scores plot
pca.scoresplot3D        3D PCA scores plot
pca.scoresplot3D.rgl    3D PCA scores plot (interactive)
pca.screeplot           PCA scree plot
peaks.per.sample        Peaks per sample
peaks.per.samples       Peaks per samples
plot.spectra            Plot spectra
plot.spectra.simple     Plot spectra (simple)
predict.samples         Predict samples
read.csvs.folder        Read CSVs from folder
read.data.csv           Read CSV data
read.dataset.csv        Read dataset from CSV
read.metadata           Read metadata
read.multiple.csvs      Read multiple CSVs
recursive.feature.elimination
                        Perform recursive feature elimination
remove.peaks.interval   Remove interval of peaks
remove.peaks.interval.sample.list
                        Remove interval of peaks (sample list)
replace.data.value      Replace data value
replace.metadata.value
                        Replace metadata's value
savitzky.golay          Savitzky-golay transformation
set.metadata            Set new metadata
set.sample.names        Set samples names
set.value.label         Set value label
set.x.label             Set x-label
set.x.values            Set new x-values
shift.correction        Shift correction
smoothing.interpolation
                        Smoothing interpolation
smoothing.spcbin.hyperspec
                        Wavelength binning
smoothing.spcloess.hyperspec
                        Loess smoothing
sum.dataset             Dataset summary
summary.var.importance
                        Summary of variables importance
tTests.dataset          t-Tests on dataset
tTests.pvalue           t-Tests on matrix
train.and.predict       Train and predict
train.classifier        Train classifier
train.models.performance
                        Train models
univariate.analysis     Univariate Analysis
values.per.peak         Values per peak
var.importance          Variables importance
variables.as.metadata   Variables as metadata
x.values.to.indexes     Get x-values indexes
xvalue.interval.to.indexes
                        Get indexes of an interval of x-values
